package game;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import java.util.Random;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
/**
 *
 * @author Andrian
 */
public class PuzzleGame extends javax.swing.JFrame {

    /** Creates new form PuzzleGame */
    
    // deklarasi objek icon setiap button dan preview
    ImageIcon ic1 = new ImageIcon(getClass().getResource("1.jpg"));
    ImageIcon ic2 = new ImageIcon(getClass().getResource("2.jpg"));
    ImageIcon ic3 = new ImageIcon(getClass().getResource("3.jpg"));
    ImageIcon ic4 = new ImageIcon(getClass().getResource("4.jpg"));
    ImageIcon ic5 = new ImageIcon(getClass().getResource("5.jpg"));
    ImageIcon ic6 = new ImageIcon(getClass().getResource("6.jpg"));
    ImageIcon ic7 = new ImageIcon(getClass().getResource("7.jpg"));
    ImageIcon ic8 = new ImageIcon(getClass().getResource("8.jpg"));
    ImageIcon ic9 = new ImageIcon(getClass().getResource("9.jpg"));
    ImageIcon ic10 = new ImageIcon(getClass().getResource("10.jpg"));
    ImageIcon ic11 = new ImageIcon(getClass().getResource("11.jpg"));
    ImageIcon ic12 = new ImageIcon(getClass().getResource("12.jpg"));
    ImageIcon icprev = new ImageIcon(getClass().getResource("akhir.jpg")); //untuk preview
    
    public PuzzleGame() {
        Counter = 0;
        initComponents();
        jlblNumOFClicks.setText(Integer.toString(Counter)); 
        
        this.setTitle("Photo Scramble - Easy");
        
        //memasukan icon ke dalam button dan label preview
        jbtnNum1.setIcon(ic10);
        jbtnNum2.setIcon(ic4);
        jbtnNum3.setIcon(ic3);
        jbtnNum4.setIcon(ic8);
        jbtnNum5.setIcon(ic6);
        jbtnNum6.setIcon(ic12);
        jbtnNum7.setIcon(ic1);
        jbtnNum8.setIcon(ic2);
        jbtnNum9.setIcon(ic5);
        jbtnNum10.setIcon(ic9);
        jbtnNum11.setIcon(ic7);
        jbtnNum12.setIcon(ic11);
        jLabel2.setIcon(icprev); // untuk preview
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jbtnNum1 = new javax.swing.JButton();
        jbtnNum2 = new javax.swing.JButton();
        jbtnNum3 = new javax.swing.JButton();
        jbtnNum4 = new javax.swing.JButton();
        jbtnNum5 = new javax.swing.JButton();
        jbtnNum6 = new javax.swing.JButton();
        jbtnNum7 = new javax.swing.JButton();
        jbtnNum8 = new javax.swing.JButton();
        jbtnNum9 = new javax.swing.JButton();
        jbtnNum10 = new javax.swing.JButton();
        jbtnNum11 = new javax.swing.JButton();
        jbtnNum12 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jlblNumOFClicks = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jBtnSolution = new javax.swing.JButton();
        jBtnAcak = new javax.swing.JButton();
        jBtnMenu = new javax.swing.JButton();
        jBtnExit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(51, 204, 255));
        setForeground(new java.awt.Color(255, 153, 102));
        setMinimumSize(new java.awt.Dimension(1420, 760));
        setPreferredSize(new java.awt.Dimension(1400, 760));
        setResizable(false);
        setSize(new java.awt.Dimension(1440, 760));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/logopuzzle.jpg"))); // NOI18N

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(456, 456, 456)
                .add(jLabel1)
                .addContainerGap(420, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 106, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 1380, 110));

        jPanel5.setBackground(new java.awt.Color(0, 204, 204));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new java.awt.GridLayout(3, 4));

        jbtnNum1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum1ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum1);

        jbtnNum2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum2ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum2);

        jbtnNum3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum3ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum3);

        jbtnNum4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum4ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum4);

        jbtnNum5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum5ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum5);

        jbtnNum6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum6ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum6);

        jbtnNum7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum7ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum7);

        jbtnNum8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum8ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum8);

        jbtnNum9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum9ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum9);

        jbtnNum10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum10ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum10);

        jbtnNum11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum11ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum11);

        jbtnNum12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNum12ActionPerformed(evt);
            }
        });
        jPanel2.add(jbtnNum12);

        jPanel5.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 122, 683, 472));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Jumlah Click");
        jLabel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jlblNumOFClicks.setFont(new java.awt.Font("Consolas", 1, 48)); // NOI18N
        jlblNumOFClicks.setForeground(new java.awt.Color(0, 0, 0));
        jlblNumOFClicks.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblNumOFClicks.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel4.setFont(new java.awt.Font("Consolas", 1, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("UBAH GAMBAR SEPERTI BERIKUT");

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jlblNumOFClicks, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 296, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .add(jPanel4Layout.createSequentialGroup()
                .add(69, 69, 69)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jLabel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
                    .add(jLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                .add(22, 22, 22)
                .add(jLabel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 74, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 42, Short.MAX_VALUE)
                .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 290, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(31, 31, 31)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jlblNumOFClicks, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
                .add(12, 12, 12))
        );

        jPanel5.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 120, 690, 590));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new java.awt.GridLayout(1, 0));

        jBtnSolution.setBackground(new java.awt.Color(0, 153, 153));
        jBtnSolution.setFont(new java.awt.Font("Consolas", 1, 24)); // NOI18N
        jBtnSolution.setForeground(new java.awt.Color(0, 0, 0));
        jBtnSolution.setText("GIVE UP");
        jBtnSolution.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnSolutionActionPerformed(evt);
            }
        });
        jPanel3.add(jBtnSolution);

        jBtnAcak.setBackground(new java.awt.Color(0, 153, 153));
        jBtnAcak.setFont(new java.awt.Font("Consolas", 1, 24)); // NOI18N
        jBtnAcak.setForeground(new java.awt.Color(0, 0, 0));
        jBtnAcak.setText("SCRAMBLE");
        jBtnAcak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAcakActionPerformed(evt);
            }
        });
        jPanel3.add(jBtnAcak);

        jBtnMenu.setBackground(new java.awt.Color(0, 153, 153));
        jBtnMenu.setFont(new java.awt.Font("Consolas", 1, 24)); // NOI18N
        jBtnMenu.setForeground(new java.awt.Color(0, 0, 0));
        jBtnMenu.setText("MENU");
        jBtnMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnMenuActionPerformed(evt);
            }
        });
        jPanel3.add(jBtnMenu);

        jBtnExit.setBackground(new java.awt.Color(0, 153, 153));
        jBtnExit.setFont(new java.awt.Font("Consolas", 1, 24)); // NOI18N
        jBtnExit.setForeground(new java.awt.Color(0, 0, 0));
        jBtnExit.setText("EXIT GAME");
        jBtnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnExitActionPerformed(evt);
            }
        });
        jPanel3.add(jBtnExit);

        jPanel5.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 630, 680, 80));

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1400, 720));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    int Counter;
         
    // digunakan untuk mengambil data icon dari setiap button sehingga dapat menghasilkan kondisi bahwa game dinyatakan menang atau tidak
    public void SolutionChecker(){
        // mendeklarasikan objek baru untuk menampung isi dari tiap button.
        Icon b1 = jbtnNum1.getIcon();
        Icon b2 = jbtnNum2.getIcon();
        Icon b3 = jbtnNum3.getIcon();
        Icon b4 = jbtnNum4.getIcon();
        Icon b5 = jbtnNum5.getIcon();
        Icon b6 = jbtnNum6.getIcon();
        Icon b7 = jbtnNum7.getIcon();
        Icon b8 = jbtnNum8.getIcon();
        Icon b9 = jbtnNum9.getIcon();
        Icon b10 = jbtnNum10.getIcon();
        Icon b11 = jbtnNum11.getIcon();
        Icon b12 = jbtnNum12.getIcon();
            
        //ketika tiap button cocok dengan objek icon yang ditentukan, maka game dinyatakan menang.
        if (b1 == ic1 && b2 == ic2 && b3 == ic3 && b4 == ic4 && b5 == ic5 && b6 == ic6 && b7 == ic7 && b8 == ic8 && b9 == ic9 && b10 == ic10 && b11 == ic11){
            JOptionPane.showMessageDialog(this, "You win the Game","Photo Scramble", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    //method yang digunakan untuk kondisi setiap button dan label.
    private void jbtnNum2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum2ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum2.getIcon();
        if(jbtnNum1.getIcon() == ic12){
            jbtnNum1.setIcon(s1);
            jbtnNum2.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum6.getIcon() == ic12){
            jbtnNum6.setIcon(s1);
            jbtnNum2.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum3.getIcon() == ic12){
            jbtnNum3.setIcon(s1);
            jbtnNum2.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum2ActionPerformed

    private void jbtnNum1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum1ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum1.getIcon();
        if(jbtnNum2.getIcon() == ic12){
            jbtnNum2.setIcon(s1);
            jbtnNum1.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum5.getIcon() == ic12){
            jbtnNum5.setIcon(s1);
            jbtnNum1.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum1ActionPerformed

    private void jbtnNum4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum4ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum4.getIcon();
        if(jbtnNum3.getIcon() == ic12){
            jbtnNum3.setIcon(s1);
            jbtnNum4.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum8.getIcon() == ic12){
            jbtnNum8.setIcon(s1);
            jbtnNum4.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum4ActionPerformed

    private void jbtnNum6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum6ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum6.getIcon();
        if(jbtnNum2.getIcon() == ic12){
            jbtnNum2.setIcon(s1);
            jbtnNum6.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum5.getIcon() == ic12){
            jbtnNum5.setIcon(s1);
            jbtnNum6.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum7.getIcon() == ic12){
            jbtnNum7.setIcon(s1);
            jbtnNum6.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum10.getIcon() == ic12){
            jbtnNum10.setIcon(s1);
            jbtnNum6.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum6ActionPerformed

    private void jbtnNum7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum7ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum7.getIcon();
        if(jbtnNum3.getIcon() == ic12){
            jbtnNum3.setIcon(s1);
            jbtnNum7.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum6.getIcon() == ic12){
            jbtnNum6.setIcon(s1);
            jbtnNum7.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum8.getIcon() == ic12){
            jbtnNum8.setIcon(s1);
            jbtnNum7.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum11.getIcon() == ic12){
            jbtnNum11.setIcon(s1);
            jbtnNum7.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum7ActionPerformed

    private void jbtnNum9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum9ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum9.getIcon();
        if(jbtnNum5.getIcon() == ic12){
            jbtnNum5.setIcon(s1);
            jbtnNum9.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum10.getIcon() == ic12){
            jbtnNum10.setIcon(s1);
            jbtnNum9.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum9ActionPerformed

    private void jbtnNum10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum10ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum10.getIcon();
        if(jbtnNum9.getIcon() == ic12){
            jbtnNum9.setIcon(s1);
            jbtnNum10.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum6.getIcon() == ic12){
            jbtnNum6.setIcon(s1);
            jbtnNum10.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum11.getIcon() == ic12){
            jbtnNum11.setIcon(s1);
            jbtnNum10.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum10ActionPerformed

    private void jbtnNum11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum11ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum11.getIcon();
        if(jbtnNum12.getIcon() == ic12){
            jbtnNum12.setIcon(s1);
            jbtnNum11.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum10.getIcon() == ic12){
            jbtnNum10.setIcon(s1);
            jbtnNum11.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum7.getIcon() == ic12){
            jbtnNum7.setIcon(s1);
            jbtnNum11.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum11ActionPerformed

    private void jbtnNum12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum12ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum12.getIcon();
        if(jbtnNum11.getIcon() == ic12){
            jbtnNum11.setIcon(s1);
            jbtnNum12.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum8.getIcon() == ic12){
            jbtnNum8.setIcon(s1);
            jbtnNum12.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum12ActionPerformed
    private JFrame frame;
    private void jBtnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnExitActionPerformed
        // TODO add your handling code here:
        frame = new JFrame("Exit");
        
        if (JOptionPane.showConfirmDialog(frame, "Do You Really Want To Quit?","Quit Photo Scramble", JOptionPane.YES_NO_OPTION)== JOptionPane.YES_NO_OPTION)
        {
            System.exit(0);
        }
    }//GEN-LAST:event_jBtnExitActionPerformed

    private void jBtnSolutionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnSolutionActionPerformed
        // TODO add your handling code here:
        jbtnNum1.setIcon(ic1);
        jbtnNum2.setIcon(ic2);
        jbtnNum3.setIcon(ic3);
        jbtnNum4.setIcon(ic4);
        jbtnNum5.setIcon(ic5);
        jbtnNum6.setIcon(ic6);
        jbtnNum7.setIcon(ic7);
        jbtnNum8.setIcon(ic8);
        jbtnNum9.setIcon(ic9);
        jbtnNum10.setIcon(ic10);
        jbtnNum11.setIcon(ic11);
        jbtnNum12.setIcon(ic12);
    }//GEN-LAST:event_jBtnSolutionActionPerformed

    private void jBtnAcakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAcakActionPerformed
        // TODO add your handling code here:
        if(jbtnNum1.getIcon() == ic12){
            jbtnNum1.setIcon(ic6);//
            jbtnNum2.setIcon(ic12);//
            jbtnNum3.setIcon(ic2);//
            jbtnNum4.setIcon(ic3);//
            jbtnNum5.setIcon(ic1);//
            jbtnNum6.setIcon(ic5);//
            jbtnNum7.setIcon(ic8);//
            jbtnNum8.setIcon(ic4);//
            jbtnNum9.setIcon(ic9);//
            jbtnNum10.setIcon(ic10);//
            jbtnNum11.setIcon(ic7);//
            jbtnNum12.setIcon(ic11);//
        }else if(jbtnNum2.getIcon() == ic12){
            jbtnNum1.setIcon(ic1);//
            jbtnNum2.setIcon(ic6);//
            jbtnNum3.setIcon(ic12);//
            jbtnNum4.setIcon(ic2);//
            jbtnNum5.setIcon(ic10);//
            jbtnNum6.setIcon(ic6);//
            jbtnNum7.setIcon(ic8);//
            jbtnNum8.setIcon(ic4);//
            jbtnNum9.setIcon(ic5);//
            jbtnNum10.setIcon(ic9);//
            jbtnNum11.setIcon(ic7);//
            jbtnNum12.setIcon(ic11);//
        }else if(jbtnNum3.getIcon() == ic12){ //
            jbtnNum1.setIcon(ic2);//
            jbtnNum2.setIcon(ic5);//
            jbtnNum3.setIcon(ic3);//
            jbtnNum4.setIcon(ic12);//
            jbtnNum5.setIcon(ic1);//
            jbtnNum6.setIcon(ic7);//
            jbtnNum7.setIcon(ic8);//
            jbtnNum8.setIcon(ic4);//
            jbtnNum9.setIcon(ic9);//
            jbtnNum10.setIcon(ic6);//
            jbtnNum11.setIcon(ic10);//
            jbtnNum12.setIcon(ic11);//
        }else if(jbtnNum4.getIcon() == ic12){ //
            jbtnNum1.setIcon(ic5);//
            jbtnNum2.setIcon(ic1);//
            jbtnNum3.setIcon(ic7);//
            jbtnNum4.setIcon(ic3);//
            jbtnNum5.setIcon(ic12);//
            jbtnNum6.setIcon(ic9);//
            jbtnNum7.setIcon(ic6);//
            jbtnNum8.setIcon(ic4);//
            jbtnNum9.setIcon(ic10);//
            jbtnNum10.setIcon(ic2);//
            jbtnNum11.setIcon(ic11);//
            jbtnNum12.setIcon(ic8);//
        }else if(jbtnNum5.getIcon() == ic12){//
            jbtnNum1.setIcon(ic1);//
            jbtnNum2.setIcon(ic2);//
            jbtnNum3.setIcon(ic3);//
            jbtnNum4.setIcon(ic8);//
            jbtnNum5.setIcon(ic6);//
            jbtnNum6.setIcon(ic12);//
            jbtnNum7.setIcon(ic4);//
            jbtnNum8.setIcon(ic7);//
            jbtnNum9.setIcon(ic5);//
            jbtnNum10.setIcon(ic9);//
            jbtnNum11.setIcon(ic10);//
            jbtnNum12.setIcon(ic11);//
        }else if(jbtnNum6.getIcon() == ic12){ //
            jbtnNum1.setIcon(ic5);//
            jbtnNum2.setIcon(ic2);//
            jbtnNum3.setIcon(ic8);//
            jbtnNum4.setIcon(ic3);//
            jbtnNum5.setIcon(ic7);//
            jbtnNum6.setIcon(ic1);//
            jbtnNum7.setIcon(ic12);//
            jbtnNum8.setIcon(ic4);//
            jbtnNum9.setIcon(ic9);//
            jbtnNum10.setIcon(ic6);//
            jbtnNum11.setIcon(ic10);//
            jbtnNum12.setIcon(ic11);//
        }else if(jbtnNum7.getIcon() == ic12){ //
            jbtnNum1.setIcon(ic1);//
            jbtnNum2.setIcon(ic3);//
            jbtnNum3.setIcon(ic8);//
            jbtnNum4.setIcon(ic4);//
            jbtnNum5.setIcon(ic9);//
            jbtnNum6.setIcon(ic2);//
            jbtnNum7.setIcon(ic11);//
            jbtnNum8.setIcon(ic12);//
            jbtnNum9.setIcon(ic6);//
            jbtnNum10.setIcon(ic5);//
            jbtnNum11.setIcon(ic10);//
            jbtnNum12.setIcon(ic7);//
        }else if(jbtnNum8.getIcon() == ic12){ //
            jbtnNum1.setIcon(ic1);//
            jbtnNum2.setIcon(ic6);//
            jbtnNum3.setIcon(ic2);//
            jbtnNum4.setIcon(ic4);//
            jbtnNum5.setIcon(ic9);//
            jbtnNum6.setIcon(ic5);//
            jbtnNum7.setIcon(ic3);//
            jbtnNum8.setIcon(ic7);//
            jbtnNum9.setIcon(ic12);//
            jbtnNum10.setIcon(ic10);//
            jbtnNum11.setIcon(ic11);//
            jbtnNum12.setIcon(ic8);//
        }else if(jbtnNum9.getIcon() == ic12){ //
            jbtnNum1.setIcon(ic3);//
            jbtnNum2.setIcon(ic5);//
            jbtnNum3.setIcon(ic4);//
            jbtnNum4.setIcon(ic8);//
            jbtnNum5.setIcon(ic9);//
            jbtnNum6.setIcon(ic2);//
            jbtnNum7.setIcon(ic7);//
            jbtnNum8.setIcon(ic6);//
            jbtnNum9.setIcon(ic10);//
            jbtnNum10.setIcon(ic12);//
            jbtnNum11.setIcon(ic1);//
            jbtnNum12.setIcon(ic11);//
        }else if(jbtnNum10.getIcon() == ic12){ //
            jbtnNum1.setIcon(ic5);//
            jbtnNum2.setIcon(ic1);//
            jbtnNum3.setIcon(ic3);//
            jbtnNum4.setIcon(ic4);//
            jbtnNum5.setIcon(ic6);//
            jbtnNum6.setIcon(ic10);//
            jbtnNum7.setIcon(ic8);//
            jbtnNum8.setIcon(ic2);//
            jbtnNum9.setIcon(ic9);//
            jbtnNum10.setIcon(ic7);//
            jbtnNum11.setIcon(ic12);//
            jbtnNum12.setIcon(ic10);//
        }else if(jbtnNum11.getIcon() == ic12){ //
            jbtnNum1.setIcon(ic5);//
            jbtnNum2.setIcon(ic1);//
            jbtnNum3.setIcon(ic4);//
            jbtnNum4.setIcon(ic8);//
            jbtnNum5.setIcon(ic9);//
            jbtnNum6.setIcon(ic2);//
            jbtnNum7.setIcon(ic11);//
            jbtnNum8.setIcon(ic7);//
            jbtnNum9.setIcon(ic10);//
            jbtnNum10.setIcon(ic3);//
            jbtnNum11.setIcon(ic6);//
            jbtnNum12.setIcon(ic12);//
        }else if(jbtnNum12.getIcon() == ic12){ //
            jbtnNum1.setIcon(ic12);//
            jbtnNum2.setIcon(ic1);//
            jbtnNum3.setIcon(ic6);//
            jbtnNum4.setIcon(ic3);//
            jbtnNum5.setIcon(ic9);//
            jbtnNum6.setIcon(ic8);//
            jbtnNum7.setIcon(ic7);//
            jbtnNum8.setIcon(ic4);//
            jbtnNum9.setIcon(ic2);//
            jbtnNum10.setIcon(ic5);//
            jbtnNum11.setIcon(ic10);//
            jbtnNum12.setIcon(ic11);//
        }
        Counter = 0;
        jlblNumOFClicks.setText(Integer.toString(Counter)); 
    }//GEN-LAST:event_jBtnAcakActionPerformed

    private void jbtnNum3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum3ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum3.getIcon();
        if(jbtnNum2.getIcon() == ic12){
            jbtnNum2.setIcon(s1);
            jbtnNum3.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum7.getIcon() == ic12){
            jbtnNum7.setIcon(s1);
            jbtnNum3.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum4.getIcon() == ic12){
            jbtnNum4.setIcon(s1);
            jbtnNum3.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum3ActionPerformed

    private void jbtnNum8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum8ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum8.getIcon();
        if(jbtnNum4.getIcon() == ic12){
            jbtnNum4.setIcon(s1);
            jbtnNum8.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum7.getIcon() == ic12){
            jbtnNum7.setIcon(s1);
            jbtnNum8.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum12.getIcon() == ic12){
            jbtnNum12.setIcon(s1);
            jbtnNum8.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum8ActionPerformed

    private void jbtnNum5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNum5ActionPerformed
        // TODO add your handling code here:
        Icon s1 = jbtnNum5.getIcon();
        if(jbtnNum1.getIcon() == ic12){
            jbtnNum1.setIcon(s1);
            jbtnNum5.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum6.getIcon() == ic12){
            jbtnNum6.setIcon(s1);
            jbtnNum5.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }else if(jbtnNum9.getIcon() == ic12){
            jbtnNum9.setIcon(s1);
            jbtnNum5.setIcon(ic12);
            Counter = Counter + 1;
            jlblNumOFClicks.setText(Integer.toString(Counter));
        }
        SolutionChecker();
    }//GEN-LAST:event_jbtnNum5ActionPerformed

    private void jBtnMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnMenuActionPerformed
        // TODO add your handling code here:
        Menu m = new Menu();
        this.dispose();
        m.setVisible(true);
    }//GEN-LAST:event_jBtnMenuActionPerformed
    //akhir dari method aksi setiap button
    
    private void formWindowActivated(java.awt.event.WindowEvent evt) {           

    } 

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PuzzleGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PuzzleGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PuzzleGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PuzzleGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PuzzleGame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAcak;
    private javax.swing.JButton jBtnExit;
    private javax.swing.JButton jBtnMenu;
    private javax.swing.JButton jBtnSolution;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JButton jbtnNum1;
    private javax.swing.JButton jbtnNum10;
    private javax.swing.JButton jbtnNum11;
    private javax.swing.JButton jbtnNum12;
    private javax.swing.JButton jbtnNum2;
    private javax.swing.JButton jbtnNum3;
    private javax.swing.JButton jbtnNum4;
    private javax.swing.JButton jbtnNum5;
    private javax.swing.JButton jbtnNum6;
    private javax.swing.JButton jbtnNum7;
    private javax.swing.JButton jbtnNum8;
    private javax.swing.JButton jbtnNum9;
    private javax.swing.JLabel jlblNumOFClicks;
    // End of variables declaration//GEN-END:variables

}
